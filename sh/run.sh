#!/usr/bin/env bash
app_name='dockerfile-maven-example'
docker stop ${app_name}
echo '----stop container----'
docker rm ${app_name}
echo '----rm container----'
docker rmi `docker images | grep none | awk '{print $3}'`
echo '----rm none images----'

mkdir -p /mount/apps/${app_name}/logs

docker run -p 8803:8803 --name ${app_name} \
-e TZ="Asia/Shanghai" \
-v /etc/localtime:/etc/localtime \
-v /mount/apps/${app_name}/logs:/logs \
-d my_df_maven/${app_name}:0.0.1-SNAPSHOT
echo '----start container----'
